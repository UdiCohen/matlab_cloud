function taggedPoints = PolygonsSplitter(x, y)

taggedPoints = NaN(length(x), 3);

lastPoint = [x(1) y(1)];

currTag = 1;

x_old = x;
y_old = y;

x_old(1) = [];
y_old(1) = [];

numOfTaggedPoints = 1;
taggedPoints(1, :) = [x(1), y(1), currTag];

% figure;
% plot(x, y, '.r');
% hold on;
% plot(lastPoint(1), lastPoint(2), '*b');
% axis equal;

while length(x_old > 0)
    
    [I, M] = dsearchn([x_old, y_old], lastPoint);
    
    if (M <= 10)
        numOfTaggedPoints = numOfTaggedPoints + 1;
        taggedPoints(numOfTaggedPoints, :) = [x_old(I), y_old(I), currTag];
        lastPoint = [x_old(I), y_old(I)];
        x_old(I) = [];
        y_old(I) = [];
%         plot(lastPoint(1), lastPoint(2), '*b');
    else
        numOfTaggedPoints = numOfTaggedPoints + 1;
        currTag = currTag + 1;
        taggedPoints(numOfTaggedPoints, :) = [x_old(I), y_old(I), currTag];
        lastPoint = [x_old(I), y_old(I)];
        x_old(I) = [];
        y_old(I) = [];
%         plot(lastPoint(1), lastPoint(2), '*b');
    end
end

for i = 1:currTag
    pointsInCurrPolygon = find(taggedPoints(:, 3) == i);
    if (length(pointsInCurrPolygon) <= 3)
        taggedPoints(pointsInCurrPolygon, :) = [];
        for j = (i+1):currTag
            pointsInCurrPolygon = find(taggedPoints(:, 3) == j);
            taggedPoints(pointsInCurrPolygon, 3) = j - 1;
        end
    end
end