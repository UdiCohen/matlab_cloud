function MC = MassCenterBW(I)

if (ndims(I) == 2)
    [i, j] = meshgrid(1:size(I, 2), 1:size(I, 1));
    weightedx = i .* I;
    weightedy = j .* I;
    MC(1) = sum(weightedx(:)) / sum(I(:));
    MC(2) = sum(weightedy(:)) / sum(I(:));
elseif (ndims(I) == 3)
    [i, j, z] = meshgrid(1:size(I, 2), 1:size(I, 1), 1:size(I, 3));
    weightedx = i .* I;
    weightedy = j .* I;
    weightedz = z .* I;
    MC(1) = sum(weightedx(:)) / sum(I(:));
    MC(2) = sum(weightedy(:)) / sum(I(:));
    MC(3) = sum(weightedz(:)) / sum(I(:));
end