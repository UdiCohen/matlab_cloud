function [moments,thickness] = compute_moment(filename_cloud,filename_load,thickness,slice,F_magnitude)

%function : compute_moment.m
%inputs : filename_cloud [string] - path to cloud file.
%         filename_load  [string] - path to two loads point file.
%         thickness [double] - thickness as measure on sorted file,
%                              insert '0' for calculate it.
%         slice[double] - the number 
%outputs: moments[vector] - 3D, moments as caluclate on CM slice.
          
%description: this function is responsible for measuring the moments on
%             random slices.
%             stages:
%             1. importing data from file
%             2. picking random slices and measuring their CM
%             3. create r,F(note F is activate on head to origin)
%             4. momentum = rXF
%               note: I scaled F magnitude to be 1000[N]
%Bugs: 1. need to check with Nir about inputs -  load(always
%           origin first and then load?)
%      2. thickness  - computing from arrange file
%      
%Author: Ehud Cohen
%Date: 23/08/2018
%Version: 1.00

%%
%Debug param
show_plot = true;
DEBUG = true;

%% import bone
    if(DEBUG)
        clc;
        close all;
        %filename_cloud = 'bone_a/PtCloudCY47yrManualA.txt';
        %filename_load = 'bone_a/LoadingPointManualA.txt';
    end

    delimiterIn = ',';
    bone = importdata(filename_cloud);
    bone_a.x = bone(:,1,:);
    bone_a.y = bone(:,2,:);
    bone_a.z = bone(:,3,:);

    if(show_plot)
            plot3(bone_a.x,bone_a.y,bone_a.z);
            hold on;
            axis equal;
    end

%% import loading point
    delimiterIn = ',';
    loads = importdata(filename_load);

    head = loads(2,:);
    origin = loads(1,:);

    if(show_plot)
        plot3(loads(:,1,:),loads(:,2,:),loads(:,3,:),'r','LineWidth',3);
    end

%%
    %creating F
    F = origin - head;                      %the power is activate on the head, with direction to the origin.
    F_mag = sqrt(F(1)^2 + F(2)^2 +F(3)^2);
    factor = F_magnitude/F_mag;                        %we want to ensure that the magnitude will be 1000[N]
    F = factor*F;

    if(show_plot)
        plot3([head(1) head(1)+F(1)],[head(2) head(2)+F(2)],[head(3) head(3)+F(3)],'g');
    end


%%
    if(~thickness) 
        %calculate slice thickness
        %I want to remove this section outside this function when finished
        %I'm assuming that in the ordered model the thickness is const
        tmp = bone_a.z;
        Z_min = min(bone_a.z);
        tmp(bone_a.z == Z_min,:) = [];
        Z_min_n = min(tmp);
        thickness = Z_min_n - Z_min;
    end
%%
    %picking 4 slice calculate the center of each of them
    Z_max = max(bone_a.z);          %normallizaing the random function to pick slice
    Z_min = min(bone_a.z);
    delta_z = Z_max - Z_min;

    if(~slice)
        num_of_slices = 4;              
    else
        num_of_slices = 1;
    end
    
    %initiliaze the center and radius storage
    C = zeros(num_of_slices,3);
    R = zeros(1,num_of_slices);

    for i=1:num_of_slices
        %some caculate to ensure that in each loop we get slice from other
        %region
        if(~slice)
            slice = Z_min + (delta_z/num_of_slices)*rand;
        end
        Z_min = Z_min + delta_z/num_of_slices;
   
   
        % thickness
        slice_points = bone(((bone_a.z<=slice + thickness/2) & (bone_a.z >= slice - thickness/2)),:);
       
        [C(i,1),C(i,2), R(i)] = compute_slice_center(slice_points(:,1),slice_points(:,2));
        C(i,3) = slice;
    end


    if(show_plot)
        plot3(C(:,1),C(:,2),C(:,3),'y');
    end

%%
    %calculate r
    r = C - head;

%%
    %calculate the moments
    moments = zeros(num_of_slices,3);

    for i=1:num_of_slices
        moments(i,:) = cross(r(i,:),F)
    end
end



