%function [moments_A,moments_B,norm_A,norm_B] = compare_moment(modelA_name,LoadA,modelB_name,LoadB,F_magnitude=1000,sliceA=0,sliceB=0) 

%%
DEBUG = true;
show_plots = false;

%%
%simulation parameter
if(DEBUG)
    clc;
    close all;

    F_magnitude = 1550; %[N]

    %if user want can determine which slice we will work on
    %choose zero for picking 4 random slice
    sliceA = 258;      
    sliceB = 358;

%% sorted model
    modelA_name = 'bone_b/PointCloudSimFiniCY47LeftC.txt';
    LoadA = 'bone_b/LoadingPointSimFiniC.txt';
    %%
    modelB_name = 'bone_b/PointCloudSimFiniCY47LeftC.txt';
    LoadB = 'bone_b/LoadingPointSimFiniC.txt';
end
%%
    [moments_A,thickness] = compute_moment(modelA_name,LoadA,1,sliceA,F_magnitude);
    norm_A = norm(moments_A);

    [moments_B,thickness] = compute_moment(modelB_name,LoadB,thickness,sliceB,F_magnitude);
    norm_B = norm(moments_B);
  
%% plots results
%end