function [xCenter,yCenter,radius] = compute_slice_center(x,y)
%function : compute_slice_center.m
%input: x - [vector] of sample on the slice contour
%       y - [vector] of sample on the slice contour
%output: xCenter [scalar] - the x value of CM slice
%        yCenter [scalar] - the y value of CM slice
%        radius - slice.
%description: this function is responsible for measuring the CM of a
%             deformation ring. the idea is to use the matrix world base on
%             distance transform: https://www.mathworks.com/help/images/distance-transform-of-a-binary-image.html
%             stages:
%             1. creating mask polygon base on the sample points
%             2. distance transform
%             3. the CM will be the points that is furthest.
%Bugs: need to rethink about the issue with two centers how to chose one of
%       them(line 66).
%Author: Ehud Cohen
%Date: 23/08/2018
%Version: 1.03


%% Debug parameter
DEBUG = false;
show_plots = false;
if(DEBUG)
    close all;
end
%% sorting the sampling
    %this is late adding so a little less effective
    taggedPoints = PolygonsSplitter(x, y);
    x = taggedPoints(:,1);
    y = taggedPoints(:,2);
%%
    %initilaze 
    xMin = min(x);
    xMax = max(x);
    yMin = min(y);
    yMax = max(y);

    scalingFactor = 1000 / min([xMax-xMin, yMax-yMin]);

    %manipulate all the vertex- for convenient 
    x2 = (x - xMin) * scalingFactor + 1;
    y2 = (y - yMin) * scalingFactor + 1;

    %create a mask base on the polygon created from the vertex.
    mask = poly2mask(x2, y2, ceil(max(y2)), ceil(max(x2))); %ceil - round toward positive infinity
    MC = MassCenterBW(mask);
    if(show_plots)
        fontSize = 25;
        % Display the image.
        p2 = subplot(2, 2, 2);
        imshow(mask);
        axis(p2, 'on', 'xy');
        title('Mask Image', 'FontSize', fontSize);
    end

%%
    % Compute the Euclidean Distance Transform
    edtImage = bwdist(~mask);

    if(show_plots)
        % Display the image.
        p3 = subplot(2, 2, 3);
        imshow(edtImage, []);
        axis(p3, 'on', 'xy');
    end 

%%
    %measuring circle propeties
    % Find the max
    radius = max(edtImage(:));
    % Find the center
    [yCenter, xCenter] = find(edtImage == radius);
    
    %prevent from slice to be with more than one center
    %this solution is bad need to fix.
    if(length(xCenter) > 1)
       xCenter = xCenter(1);
       yCenter = yCenter(1);
    end

    if(show_plots)
        % Display circles over edt image.
        viscircles(p3, [xCenter, yCenter], radius);
        % Display polygon over image also.
        hold on;
        plot(x2, y2, 'r.-', 'MarkerSize', 30, 'LineWidth', 2);
        title('Euclidean Distance Transform with Circle on it', 'FontSize', fontSize);
        % Display the plot again.
        subplot(2, 2, 4);
        plot(x, y, 'b.-', 'MarkerSize', 30);
        grid on;
        % Show the circle on it.
        hold on;
    end 

%%
% Scale and shift the center back to the original coordinates.
    xCenter = (xCenter - 1)/ scalingFactor + xMin
    yCenter = (yCenter - 1)/ scalingFactor + yMin
    radius = radius / scalingFactor;
    MC(1) = (MC(1) - 1)/scalingFactor + xMin
    MC(2) = (MC(2) - 1)/scalingFactor + yMin
    
    xCenter = MC(1);
    yCenter = MC(2);
    
    if(show_plots)
        rectangle('Position',[xCenter-radius, yCenter-radius, 2*radius, 2*radius],'Curvature',[1,1]);
        title('Original Points with Inscribed Circle', 'FontSize', fontSize);
    end
end

